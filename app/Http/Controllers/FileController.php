<?php

namespace App\Http\Controllers;

use App\Http\Requests\FileRequest;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files=File::all();
        return view('cms.admin.files.index',['files'=>$files]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          if (ControllersService::checkPermission('create-file', 'user')

            || Auth::user()->type=='admin') {
        return view('cms.admin.files.create');
            }
             return view('cms.admin.blocked');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FileRequest $request)
    {
        if (ControllersService::checkPermission('create-file', 'user')

            || Auth::user()->type=='admin') {

        $file=new File();
        $file->title=$request->title;
        $file->content=$request->content;
        $file->save();
          SuccessError::Success('Create Successfuly');
        return Redirect()->back();

            }
             return view('cms.admin.blocked');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if (ControllersService::checkPermission('show-file', 'user')

            || Auth::user()->type=='admin') {
        $file=File::find($id);
        return view('cms.admin.files.show',['file'=>$file]);

            }
             return view('cms.admin.blocked');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (ControllersService::checkPermission('update-file', 'user')

            || Auth::user()->type=='admin') {
        // dd(45);
        $file=File::find($id);
        return view('cms.admin.files.edit',['file'=>$file]);
        //
            }
             return view('cms.admin.blocked');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (ControllersService::checkPermission('update-file', 'user')

            || Auth::user()->type=='admin') {
        $file= File::find($id);
        $file->title=$request->title;
        $file->content=$request->content;
        $file->save();
          SuccessError::Success('Update Successfuly');
        return Redirect()->back();

            }
             return view('cms.admin.blocked');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (ControllersService::checkPermission('delete-file', 'user')

            || Auth::user()->type=='admin') {
        $delet=    $contactUser=file::find($id)->delete();
        if($delet){

        return response()->json(['icon' => 'success', 'title' => 'File Deleted successfully'], 200);

    }

            return response()->json(['icon' => 'error', 'title' => 'File created failed'], 400);
}
 return view('cms.admin.blocked');

    }
}
