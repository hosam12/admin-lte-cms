<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    public function index(){
if(Auth::user()->type=='admin'){

        $users=User::where('type','!=','admin')->get();
        return view('cms.admin.users.index',['users'=>$users]);
    }

 return view('cms.admin.blocked');
    }

    public function create(){
if(Auth::user()->type=='admin'){
        return view('cms.admin.users.create');
    }
     return view('cms.admin.blocked');
}


    public function store(UserRequest $request){
if(Auth::user()->type=='admin'){
        $user=new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=Hash::make($request->get('password'));
        $user->gender=$request->get('gender');
        $user->search = Hash::make(Str::random(30));

        $user->save();

        SuccessError::Success('Create Successfuly');
        return Redirect()->back();

}

 return view('cms.admin.blocked');

    }

    public function edit($id){
        if(Auth::user()->type=='admin'){
       $user=User::where('type','!=','admin')->where('id',$id)->first();
       if($user){
 return view('cms.admin.users.edit',['user'=>$user]);
       }
        return view('cms.admin.blocked');

    }
     return view('cms.admin.blocked');
    }



    public function update(UserRequest $request,$id){

        if(Auth::user()->type=='admin'){

        // dd(45);
            $user=User::where('type','!=','admin')->where('id',$id)->first();
if($user){
        $user->name=$request->name;
        $user->email=$request->email;
        if($request->password){
        $user->password=Hash::make($request->get('password'));

        }
        $user->gender=$request->get('gender');


        $user->save();

        SuccessError::Success('Update Successfuly');
        return Redirect()->back();

    }



   }
        return view('cms.admin.blocked');

    }


    public function destroy($id){
            if(Auth::user()->type=='admin'){
      $delet=    $contactUser=User::find($id)->delete();
        if($delet){

        return response()->json(['icon' => 'success', 'title' => 'User Deleted successfully'], 200);

    }

            return response()->json(['icon' => 'error', 'title' => 'User created failed'], 400);

}
    }

       public function editPermissions(Request $request, $id)
    {
if (Auth::user()->type=='admin') {
        $user = User::find($id);
        $adminPermissions = $user->permissions()->get();
        $permissions = Permission ::where('guard_name', 'user')->get();
        return view('cms.admin.users.userPermission', [
            'user' => $user,
            'permissions' => $permissions,
            'adminPermissions' => $adminPermissions]);
    }
                return view('cms.admin.blocked');

}

    public function updatePermissions(Request $request, $id)
    {
        if (Auth::user()->type=='admin') {
        $request->request->add(['id' => $id]);
        $request->validate([
            'id' => 'required|exists:users,id',
            'permissions' => 'array',
        ]);

        $user = User::find($request->get('id'));
        $permissions = Permission::findMany($request->permissions);
        $isSynced = $user->syncPermissions($permissions);
        if ($isSynced) {
            return response()->json(['icon' => 'success', 'title' => 'User permissions update failed'], 200);
        } else {
            return response()->json(['icon' => 'Failed', 'title' => 'Admin permissions update failed'], 400);
        }
    }
    }


      public function editRoles(Request $request, $id)
    {
            if (Auth::user()->type=='admin') {
        $user = User::find($id);

        $adminRoles = $user->Roles()->get();
        $Roles = Role::where('guard_name', 'user')->get();
        return view('cms.admin.users.userRoles', [
            'user' => $user,
            'roles' => $Roles,
            'adminRoles' => $adminRoles]);
    }
                    return view('cms.admin.blocked');

}

    public function updateRoles(Request $request, $id)
    {
          if (Auth::user()->type=='admin') {
        $request->request->add(['id' => $id]);
        $request->validate([
            'id' => 'required|exists:users,id',
            'roles' => 'array',
        ]);

        $user = User::find($request->get('id'));
        $roles = Role::findMany($request->roles);
        $isSynced = $user->syncRoles($roles);
        // $isSynced=1;
        if ($isSynced) {
            return response()->json(['icon' => 'success', 'title' => 'User permissions update Successfuly'], 200);
        } else {
            return response()->json(['icon' => 'Failed', 'title' => 'Admin permissions update failed'], 400);
        }
    }
     return view('cms.admin.blocked');
    }
}

