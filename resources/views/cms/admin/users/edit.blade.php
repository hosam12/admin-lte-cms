@extends('cms.admin.parent')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>User - Update</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}">Home</a></li>
                        <li class="breadcrumb-item active">Update User</li>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Update User</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" method="post" action="{{route('user.update',$user->id)}}">
                            @method('put')
                            @csrf

                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                @if (session()->has('message'))
                                <div class="alert {{session()->get('status')}} alert-dismissible fade show" role="alert">
                                    <span> {{ session()->get('message') }}</span>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Full Name</label>
                                    <input name="name" value="{{$user->name}}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Email</label>
                                    <input name="email" value="{{$user->email}}" type="email" class="form-control" id="exampleInputPassword1" placeholder="Enter email">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Change Password</label>

                                    <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Enter password">


                                </div>
                                <input hidden value={{$user->id}} name="id">




                                <div class="form-group">
                                    <label for="exampleInputPassword1">Gender</label>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" id="Male" name="gender" value="Male" @if($user->gender=='Male' ) checked @endif>
                                        <label for="Male" class="custom-control-label">Male</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" id="Female" name="gender" value="Female" @if($user->gender=='Female' ) checked @endif>

                                        <label for="Female" class="custom-control-label">Female</label>
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection

@section('script')

@endsection

