<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Security</title>


    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('cms/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('cms/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('cms/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>


        </ul>

        <!-- Right navbar links -->
<ul class="navbar-nav ml-auto">
    <!-- Notifications Dropdown Menu -->


    <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fas fa-user"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

            <div class="dropdown-divider"></div>
            <a href="{{ route('admin.password_reset_view') }}" class="dropdown-item">
                <i class="fas fa-key mr-2"></i> Change Password
                {{-- <span class="float-right text-muted text-sm">2 days</span> --}}
            </a>


            <div class="dropdown-divider"></div>
            <a href="{{ route('admin.logout') }}" class="dropdown-item">
                <i class="fas fa-power-off mr-2"></i> Log Out
                {{-- <span class="float-right text-muted text-sm">12 hours</span> --}}
            </a>
            <div class="dropdown-divider"></div>
            {{-- <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a> --}}
        </div>
    </li>



</ul>

    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
            <img src="{{asset('cms/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
                 class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">Security</span>

        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{asset('cms/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2"
                         alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">{{Auth::user()->name}}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->


@if(Auth::user()->type=='admin')


                          <li class="nav-item has-treeview">
                              <a href="#" class="nav-link">
                                  <i class="nav-icon fas fa-users"></i>
                                  <p>
                                      Users
                                      <i class="right fas fa-angle-left"></i>
                                  </p>
                              </a>
                              <ul class="nav nav-treeview">

                                  <li class="nav-item">
                                      <a href="{{route('user.index')}}" class="nav-link">
                                          <i class="far fa-circle nav-icon"></i>
                                          <p>Index</p>
                                      </a>
                                  </li>


                                  <li class="nav-item">
                                      <a href="{{route('user.create')}}" class="nav-link">
                                          <i class="far fa-circle nav-icon"></i>
                                          <p>Create</p>
                                      </a>
                                  </li>

                              </ul>
                          </li>
                          @endif




                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-pen"></i>
                                <p>
                                    Files
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                    <li class="nav-item">
                                        <a href="{{route('file.index')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Index</p>
                                        </a>
                                    </li>

@if(auth()->user()->can('create-file')||Auth::user()->type=='admin')

                                    <li class="nav-item">
                                        <a href="{{route('file.create')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Create</p>
                                        </a>
                                    </li>
                                    @endif

                            </ul>
                        </li>



   @if(Auth::user()->type=='admin')

   {{-- @if(auth()->user()->can('create-role') || auth()->user()->can('read-roles') ||
                            auth()->user()->can('create-permission') || auth()->user()->can('read-permission')) --}}
   <li class="nav-header">Permission & Roles</li>
   {{-- @if(auth()->user()->can('create-role') || auth()->user()->can('read-roles')) --}}

   <li class="nav-item has-treeview">
       <a href="#" class="nav-link">
           <i class="nav-icon fas fa-signature"></i>
           <p>
               Roles

               <i class="right fas fa-angle-left"></i>
           </p>
       </a>
       <ul class="nav nav-treeview">
           {{-- @if(auth()->user()->can('read-roles')|| Auth::guard('admin')->check()) --}}
           <li class="nav-item">
               <a href="{{route('roles.index')}}" class="nav-link">
                   <i class="far fa-circle nav-icon"></i>
                   <p>All Roles</p>

               </a>
           </li>
           {{-- @endif --}}
           {{-- @endcan --}}
           {{-- @if(auth()->user()->can('create-role')|| Auth::guard('admin')->check()) --}}

           <li class="nav-item">
               <a href="{{route('roles.create')}}" class="nav-link">
                   <i class="far fa-circle nav-icon"></i>
                   <p>New Roles</p>

               </a>
           </li>
           {{-- @endif --}}

           {{-- @endcan --}}
       </ul>
   </li>


   {{-- @if(auth()->user()->can('read-permissions')|| Auth::guard('admin')->check()) --}}
   <li class="nav-item has-treeview">
       <a href="#" class="nav-link">
           <i class="nav-icon fas fa-sign"></i>
           <p>
               Permission
               <i class="right fas fa-angle-left"></i>
           </p>
       </a>
       <ul class="nav nav-treeview">
           {{-- @can('read-permissions') --}}
           <li class="nav-item">
               <a href="{{route('permissions.index')}}" class="nav-link">
                   <i class="far fa-circle nav-icon"></i>
                   <p>All Permission</p>
               </a>
           </li>

   </ul>
   </li>
  @endif


<li class="nav-header">Settings</li>



   <li class="nav-item has-treeview">
       <a href="#" class="nav-link">
           <i class="nav-icon fas fa-cog"></i>
           <p>
               Settings
               <i class="right fas fa-angle-left"></i>
           </p>
       </a>


       <ul class="nav nav-treeview">
           {{-- @can('read-permissions') --}}
           <li class="nav-item">
               <a href="{{ route('admin.password_reset_view') }}" class="nav-link">

                   <i class="fas fa-key mr-2 nav-icon"></i>

                   <p>Change Password</p>

               </a>
           </li>












                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <@yield('content')
<!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->

</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{asset('cms/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('cms/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('cms/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('cms/dist/js/adminlte.js')}}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{asset('cms/dist/js/demo.js')}}"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">



<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>



@yield('script')
</body>
</html>
