@extends('cms.admin.parent')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Files</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="#" onClick="history.go(-1)">Back</a></li>




                            <li class="breadcrumb-item active">Files</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Files</h3>

                            {{-- @can('create-category') --}}
                            @if(auth()->user()->can('create-file')||Auth::user()->type=='admin')

                                <a href="{{route('file.create')}}" class="btn btn-sm btn-info float-right">Create
                                    New
                                    File</a>
                                    @endif
                            {{-- @endcan --}}
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>

                                    <th>Title</th>

                                                          @if(auth()->user()->can('update-file')||auth()->user()->can('show-file')||auth()->user()->can('delete-file') || Auth::user()->type=='admin')


                                    <th>Settings</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                <span hidden>{{$count = 0}}</span>
                                @foreach($files as $item)
                                    <tr>
                                        <td><span class="badge badge-info">{{++$count}}</span></td>


                                        <td>{{$item->title}}</td>






                                                          @if(auth()->user()->can('update-file')||auth()->user()->can('show-file')||auth()->user()->can('delete-file') || Auth::user()->type=='admin')





                                        <td>


 @if(auth()->user()->can('delete-file')||Auth::user()->type=='admin')

                                                <a href="#" onclick="confirmDelete(this, '{{$item->id}}')"

                                                   class="btn btn-xs btn-danger" style="color: white;">Delete</a>
@endif
 @if(auth()->user()->can('update-file')||Auth::user()->type=='admin')


                                                <a href="{{route('file.edit',[$item->id])}}"


                                                   class="btn btn-xs btn-info" style="color: white;">Edit</a>

@endif
 @if(auth()->user()->can('show-file')||Auth::user()->type=='admin')


                                                    <a href="{{route('file.show',[$item->id])}}" class="btn btn-xs btn-success" style="color: white;">Show</a>
@endif

                                            {{-- @endcan --}}
                                        </td>

                                        @endif

                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                       <th>#</th>

                                       <th>Title</th>

                                                          @if(auth()->user()->can('update-file')||auth()->user()->can('show-file')||auth()->user()->can('delete-file') || Auth::user()->type=='admin')


                                       <th>Settings</th>
                                       @endif

                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        {{-- <div class="row justify-content-center">
                            {{$categoriesData->render()}}
                        </div> --}}
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')


    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
        function confirmDelete(app, id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    deleteCategory(app, id)
                }
            })
        }

        function deleteCategory(app, id) {
            axios.delete('/cms/admin/file/' + id)
                .then(function (response) {
                    // handle success (Status Code: 200)
                    console.log(response);
                    console.log(response.data);
                    showMessage(response.data);
                    app.closest('tr').remove();
                })
                .catch(function (error) {
                    // handle error (Status Code: 400)
                    console.log(error);
                    console.log(error.response.data);
                    showMessage(error.response.data);
                })
                .then(function () {
                    // always executed
                });
        }

        function showMessage(data) {
            Swal.fire({
                position: 'center',
                icon: data.icon,
                title: data.title,
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>
@endsection
